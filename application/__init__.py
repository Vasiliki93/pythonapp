'''
@author: vgoritsas
'''
from flask import Flask
from flask_mongoalchemy import MongoAlchemy


app = Flask(__name__)
app.config['MONGOALCHEMY_DATABASE'] = 'pythonapp'
app.config['MONGOALCHEMY_CONNECTION_STRING'] = 'mongodb://admin_usr:admin123@ds111718.mlab.com:11718/pythonapp'
app.config['DEBUG'] = True
db = MongoAlchemy(app)


from application import views

_ = views

from  application import  route_api
_ = route_api

