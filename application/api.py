import os
from bson.json_util import dumps
from application.collection import  cars as ControllerCar
from flask import jsonify, request , redirect , url_for


APP_ROOT = os.path.dirname(os.path.abspath(__file__))
#display data
def displayCars():

    fetch_all_cars = ControllerCar.query.all()
    list = []
    for item in fetch_all_cars:
        list.append(item.wrap())
    if not list:
        return jsonify({'error': 'no found results'})
    else:
        return dumps(list)
#insert data
def post():
    if  request.method == 'POST':
        name = request.form['nameis']
        initial_price = request.form['initial_price']
        num_of_pass=request.form['num_of_pass']
        final_price=request.form['final_price']
        description =request.form['description']
        type_of_car =request.form['type_of_car']
        is_top = request.form['is_top']

        target = os.path.join(APP_ROOT, 'static/images/')
        if not os.path.isdir(target):
            os.mkdir(target)

        if 'file' not in request.files:
            print ('no file')

        file = request.files['file']
        filename = file.filename

        destinasion = "/".join([target, filename])
        file.save(destinasion)

        car = ControllerCar(name=name, initial_price=initial_price, num_of_pass=num_of_pass,
                            final_price=final_price, description=description, type_of_car=type_of_car,image = filename , is_top=is_top )
        car.save()
        return redirect(url_for('add_car'))


##search by george michalopoulos
##methodos search
##dimiourgei lista
#fernei ta dedomena se json gia na emfanistoun sti selida

def search(name, final_price):
    search_cars = ControllerCar.query.filter(ControllerCar.name == name, ControllerCar.final_price == final_price)

    list = []
    for item in search_cars:
        list.append(item.wrap())
    if not list:
        return jsonify({'error': 'no found results'})
    else:
        return dumps(search_cars)